$(function () {

    Fancybox.bind('.js__modal', {
        autoFocus: false,
        trapFocus: false,
        dragToClose:false,
        closeButton: 'inside',
        touch: false
    });


    $(".js_select").each(function (index, element) {
		let placeholder = $(this).attr('placeholder');
		$(this).select2({
			minimumResultsForSearch: 1 / 0,
			placeholder: placeholder,
			allowClear: true
		})
	});


    $(window).on("load",function(){
        $('body').addClass('loaded')
    })

    $('.js_parking_filter__add_btn').click(function(){
        $(this).toggleClass('active')
        if ($(this).hasClass('active')){
            $(this).find('span').text('Clear')
        }
        else{
            $(this).find('span').text('Advanced options')
        }
        $('.js_parking_filter__parking_add').slideToggle(400);
    })


    $('.js_tab_link a').click(function(){
        if (!$(this).hasClass('active')){
            $('.js_tab_link a.active').removeClass('active')
            let ind = $(this).toggleClass('active').index();
            let $TabContent = $(this).closest('.js_tab_wrap').find('.js_tab_item').eq(ind);

            $(this).closest('.js_tab_wrap').find('.js_tab_item.active').stop().slideUp(500, function(){
                $(this).removeClass('active')
                $TabContent.stop().slideDown(500,function(){
                    $(this).addClass('active')
                })
            });
        }
    })


    $('.js_form__field--distance .form__field--distance__btn').click(function(){
        let val = parseInt($(this).data('value'))
        let newVal = parseInt($('.js_input_distance').val())+val>0 ? parseInt($('.js_input_distance').val())+val : 0;
        $('.js_input_distance').val(newVal)
    })





    $('.js_avaliable_type').change(function(){
        if ($(this).val() == 'limited'){
            $(this).closest('.js_block__space').find('.js_form__block__available').stop().slideDown(400)
        }
        else{
            $(this).closest('.js_block__space').find('.js_form__block__available').stop().slideUp(400)
        }
    })

    $('.js_form__block__available__checkbox').change(function(){
        if ($(this).prop('checked')){
            $(this).closest('.form__block__available__item').find('.form__block__available__item__line').addClass('active')
        }
        else{
            $(this).closest('.form__block__available__item').find('.form__block__available__item__line').removeClass('active')
        }
    })







    $('.js_custom_select .custom_select__view').click(function(){
        $(this).closest('.js_custom_select').toggleClass('active')
    })


    $('.js_custom_select .custom_select__list li').click(function(){
        $(this).closest('.js_custom_select').find('.custom_select__view__value').text($(this).text())
        $(this).closest('.js_custom_select').toggleClass('active')
    })


    $('body').click(function (event) {
		if (!$(event.target).closest('.js_custom_select').length && $('.js_custom_select').hasClass('active')){
			$('.js_custom_select').removeClass('active')
		}
		if (!$(event.target).closest('.js_archive__top__info__order').length && $('.js_archive__top__info__order').hasClass('active')){
			$('.js_archive__top__info__order').toggleClass('active')
		}
		if (!$(event.target).closest('.js_page__area__top__slider').length && $('.js_page__area__top__slider').hasClass('active')){
			$('.js_page__area__top__slider').toggleClass('active')
		}
	})



    $('.js_form__field--password .form__field__eye').click(function(){
        $(this).toggleClass('visible')
        if ($(this).closest('.js_form__field--password').find('input').attr('type')=='password'){
            $(this).closest('.js_form__field--password').find('input').attr('type', 'text')
        }
        else{
            $(this).closest('.js_form__field--password').find('input').attr('type', 'password')
        }
    })

    $('.js__btn_close__modal').click(function (e) {
        Fancybox.close();
    });


    $('.js_btn_menu,.js_modal__menu__btn_close,.js_modal__menu__overlay').click(function (e) {
        e.preventDefault();
        $('.js_btn_menu').toggleClass('active')
        $('.js__modal__menu').toggleClass('active')
        $('.js_modal__menu__overlay').toggleClass('active')
    });

});